package com.tsystems.javaschool.tasks.calculator;

import java.util.Stack;

public class RPN {
    private Stack<Character> stack = new Stack<>();
    private StringBuilder output = new StringBuilder();

    public String evaluate(String input){
        for(int i = 0; i < input.length(); i++){
            char symbol = input.charAt(i);
            if(symbol == ' '){
                continue;
            }
            if (symbol == '+' || symbol == '-') {
                output.append(" ");
                operator(symbol, 1);
                continue;
            }
            if(symbol == '*' || symbol == '/'){
                output.append(" ");
                operator(symbol, 2);
                continue;
            }
            if(symbol == '('){
                stack.push(symbol);
                continue;
            }
            if(symbol == ')'){
                if(!parenthesisComplete()){
                   return "";
                }
            }else{
                output.append(symbol);
            }
        }
        while (!stack.isEmpty()){
            output.append(" ").append(stack.pop());
        }
        return output.toString();
    }

    private void operator(char oper, int priority){
        while (!stack.isEmpty()){
            char top = stack.pop();
            if(top == '('){
                stack.push(top);
                break;
            }else{
                int prior2;
                if(top == '+' || top == '-'){
                    prior2 = 1;
                }else {
                    prior2 = 2;
                }

                if(prior2 < priority){
                    stack.push(top);
                    break;
                }else{
                    output.append(top).append(" ");
                }
            }
        }
        stack.push(oper);
    }

    private boolean parenthesisComplete(){
        if(stack.size() < 2){
            return false;
        }
        while (!stack.isEmpty()){
            char top = stack.pop();
            if(top == '('){
                break;
            }else{
                output.append(" ").append(top);
            }
       }
       return true;
    }
}
