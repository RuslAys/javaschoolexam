package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Stack;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if(statement == null) return null;

        Stack<Double> stack = new Stack<>();

        RPN converterToRPN = new RPN();
        String postfix = converterToRPN.evaluate(statement);

        if(postfix.isEmpty()) return null;
        String[] separatedStatement = postfix.split(" ");

        for(String s : separatedStatement){
            if(s.isEmpty()) return null;
            if(!isOperator(s.charAt(0))){
                double value;
                try {
                    value = Double.parseDouble(s);
                }catch (NumberFormatException e){
                    return null;
                }
                stack.push(value);
            }else {
                double secondNumber = stack.pop();
                double firstNumber = stack.pop();
                double tmpRes = makeOperation(
                        firstNumber,
                        secondNumber,
                        s);
                if(Double.isNaN(tmpRes)){
                    return null;
                }
                stack.push(tmpRes);
            }
        }
        NumberFormat nf = new DecimalFormat("#.####");
        double number = stack.pop();
        return nf.format(number);
    }

    private boolean isOperator(char c){
        switch (c){
            case '+':
            case '-':
            case '*':
            case '/':
                return true;
        }
        return false;
    }

    private double makeOperation(double first, double second, String operator){
        switch (operator.charAt(0)){
            case '+':
                return first + second;
            case '-':
                return first - second;
            case '*':
                return first * second;
            case '/':
                if(second == 0){
                    return Double.NaN;
                }
                return first / second;
        }
        return 0;
    }

}
