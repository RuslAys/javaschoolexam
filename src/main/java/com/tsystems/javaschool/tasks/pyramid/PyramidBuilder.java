package com.tsystems.javaschool.tasks.pyramid;

import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here
        if(inputNumbers.contains(null)){
            throw new CannotBuildPyramidException();
        }

        int size = inputNumbers.size();

        long count = 0, rows = 1;

        while (count < size){
            count = count + (rows++);
        }

        if(size == count){
            rows--;
            int cols = (2 * (int)rows) - 1;
            int[][] result = new int[(int)rows][cols];

            inputNumbers.sort(
                    (Integer i1, Integer i2)-> i1 - i2);

            int center = (cols / 2);
            count = 1;
            int sortedListIdx = 0;
            int offset = 0;

            for(int i = 0; i < rows; i++){
                int start = center - offset;
                for(int j = 0; j < count * 2; j += 2){
                    result[i][start + j] = inputNumbers.get(sortedListIdx);
                    sortedListIdx++;
                }
                offset++;
                count++;
            }
            return result;
        }else {
            throw new CannotBuildPyramidException();
        }
    }


}
